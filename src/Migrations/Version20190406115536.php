<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190406115536 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_62615BA28C25BF2');
        $this->addSql('DROP INDEX IDX_62615BAB928BA2A');
        $this->addSql('CREATE TEMPORARY TABLE __temp__matches AS SELECT id, equipe_locale_id, equipe_adverse_id, domicile_exterieur, date_heure, num_semaine, num_journee, gymnase FROM matches');
        $this->addSql('DROP TABLE matches');
        $this->addSql('CREATE TABLE matches (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, equipe_locale_id INTEGER DEFAULT NULL, equipe_adverse_id INTEGER DEFAULT NULL, domicile_exterieur SMALLINT NOT NULL, date_heure DATETIME DEFAULT NULL, num_semaine INTEGER DEFAULT NULL, num_journee INTEGER DEFAULT NULL, gymnase VARCHAR(64) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_62615BAB928BA2A FOREIGN KEY (equipe_locale_id) REFERENCES equipes (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_62615BA28C25BF2 FOREIGN KEY (equipe_adverse_id) REFERENCES equipes (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO matches (id, equipe_locale_id, equipe_adverse_id, domicile_exterieur, date_heure, num_semaine, num_journee, gymnase) SELECT id, equipe_locale_id, equipe_adverse_id, domicile_exterieur, date_heure, num_semaine, num_journee, gymnase FROM __temp__matches');
        $this->addSql('DROP TABLE __temp__matches');
        $this->addSql('CREATE INDEX IDX_62615BA28C25BF2 ON matches (equipe_adverse_id)');
        $this->addSql('CREATE INDEX IDX_62615BAB928BA2A ON matches (equipe_locale_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_62615BAB928BA2A');
        $this->addSql('DROP INDEX IDX_62615BA28C25BF2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__matches AS SELECT id, equipe_locale_id, equipe_adverse_id, domicile_exterieur, date_heure, num_semaine, num_journee, gymnase FROM matches');
        $this->addSql('DROP TABLE matches');
        $this->addSql('CREATE TABLE matches (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, equipe_locale_id INTEGER DEFAULT NULL, equipe_adverse_id INTEGER DEFAULT NULL, domicile_exterieur SMALLINT NOT NULL, date_heure DATETIME DEFAULT NULL, num_semaine INTEGER DEFAULT NULL, num_journee INTEGER DEFAULT NULL, gymnase VARCHAR(64) DEFAULT NULL)');
        $this->addSql('INSERT INTO matches (id, equipe_locale_id, equipe_adverse_id, domicile_exterieur, date_heure, num_semaine, num_journee, gymnase) SELECT id, equipe_locale_id, equipe_adverse_id, domicile_exterieur, date_heure, num_semaine, num_journee, gymnase FROM __temp__matches');
        $this->addSql('DROP TABLE __temp__matches');
        $this->addSql('CREATE INDEX IDX_62615BAB928BA2A ON matches (equipe_locale_id)');
        $this->addSql('CREATE INDEX IDX_62615BA28C25BF2 ON matches (equipe_adverse_id)');
    }
}
