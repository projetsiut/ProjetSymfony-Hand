<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190409135205 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Numeros DROP FOREIGN KEY FK_C3F11235FCEC9EF');
        $this->addSql('DROP TABLE Communes');
        $this->addSql('DROP TABLE Contacts');
        $this->addSql('DROP TABLE Controles');
        $this->addSql('DROP TABLE Coureurs');
        $this->addSql('DROP TABLE Departements');
        $this->addSql('DROP TABLE Distances');
        $this->addSql('DROP TABLE Numeros');
        $this->addSql('DROP TABLE Parcours');
        $this->addSql('DROP TABLE Passages');
        $this->addSql('DROP TABLE Personnes');
        $this->addSql('DROP TABLE Regions');
        $this->addSql('DROP TABLE Utilisateurs');
        $this->addSql('ALTER TABLE matches DROP FOREIGN KEY FK_62615BA28C25BF2');
        $this->addSql('ALTER TABLE matches DROP FOREIGN KEY FK_62615BAB928BA2A');
        $this->addSql('DROP INDEX IDX_62615BA28C25BF2 ON matches');
        $this->addSql('DROP INDEX IDX_62615BAB928BA2A ON matches');
        $this->addSql('ALTER TABLE matches ADD equipe_locale INT DEFAULT NULL, ADD equipe_adverse INT DEFAULT NULL, DROP equipe_locale_id, DROP equipe_adverse_id');
        $this->addSql('ALTER TABLE matches ADD CONSTRAINT FK_62615BA4CB7A030 FOREIGN KEY (equipe_locale) REFERENCES equipes (id)');
        $this->addSql('ALTER TABLE matches ADD CONSTRAINT FK_62615BAEF1DD85D FOREIGN KEY (equipe_adverse) REFERENCES equipes (id)');
        $this->addSql('CREATE INDEX IDX_62615BA4CB7A030 ON matches (equipe_locale)');
        $this->addSql('CREATE INDEX IDX_62615BAEF1DD85D ON matches (equipe_adverse)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Communes (id CHAR(5) NOT NULL COLLATE utf8_general_ci, région INT NOT NULL, libelléMAJ VARCHAR(64) NOT NULL COLLATE utf8_general_ci COMMENT \'En majuscules sans accent\', libellé VARCHAR(64) NOT NULL COLLATE utf8_general_ci COMMENT \'Minuscules accentuées\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Contacts (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(64) NOT NULL COLLATE utf8_general_ci, prénom VARCHAR(64) DEFAULT NULL COLLATE utf8_general_ci, tél VARCHAR(16) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'Liste des contacts\' ');
        $this->addSql('CREATE TABLE Controles (id INT AUTO_INCREMENT NOT NULL, localisation VARCHAR(64) DEFAULT \'\' COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Coureurs (dossard INT NOT NULL, nom VARCHAR(64) DEFAULT \'\' COLLATE utf8_general_ci, villeOuClub VARCHAR(128) DEFAULT \'\' COLLATE utf8_general_ci, puce INT NOT NULL, photo VARCHAR(255) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(dossard)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Departements (id VARCHAR(3) NOT NULL COLLATE utf8_general_ci COMMENT \'Identifiant\', région INT NOT NULL COMMENT \'Région de rattachement\', chefLieu CHAR(5) NOT NULL COLLATE utf8_general_ci COMMENT \'Chef-Lieu (Commune)\', libelléMAJ VARCHAR(64) NOT NULL COLLATE utf8_general_ci COMMENT \'En majuscules sans accent\', libellé VARCHAR(64) NOT NULL COLLATE utf8_general_ci COMMENT \'Minuscules accentuées\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Distances (parcours INT NOT NULL, contrôle INT NOT NULL, kms INT NOT NULL, PRIMARY KEY(parcours, contrôle)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Numeros (id INT AUTO_INCREMENT NOT NULL, personne INT DEFAULT NULL, type VARCHAR(32) NOT NULL COLLATE utf8mb4_unicode_ci, numero VARCHAR(16) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_C3F11235FCEC9EF (personne), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Parcours (id INT AUTO_INCREMENT NOT NULL, libellé VARCHAR(64) DEFAULT \'\' COLLATE utf8_general_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Passages (contrôle INT NOT NULL, puce INT NOT NULL, horaire DATETIME NOT NULL, PRIMARY KEY(contrôle, puce)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Personnes (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci, prenom VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE Regions (id INT NOT NULL COMMENT \'Identifiant\', chefLieu CHAR(5) NOT NULL COLLATE utf8_general_ci COMMENT \'Chef-Lieu (Commune)\', libelléMAJ VARCHAR(64) NOT NULL COLLATE utf8_general_ci COMMENT \'En majuscules sans accent\', libellé VARCHAR(64) NOT NULL COLLATE utf8_general_ci COMMENT \'Minuscules accentuées\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'Liste des régions de France 2015\' ');
        $this->addSql('CREATE TABLE Utilisateurs (login VARCHAR(16) NOT NULL COLLATE utf8_general_ci, mdp VARCHAR(256) NOT NULL COLLATE utf8_general_ci, nom VARCHAR(64) NOT NULL COLLATE utf8_general_ci, prenom VARCHAR(64) DEFAULT NULL COLLATE utf8_general_ci, role VARCHAR(64) DEFAULT NULL COLLATE utf8_general_ci, PRIMARY KEY(login)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'Liste des utilisateurs\' ');
        $this->addSql('ALTER TABLE Numeros ADD CONSTRAINT FK_C3F11235FCEC9EF FOREIGN KEY (personne) REFERENCES Personnes (id)');
        $this->addSql('ALTER TABLE matches DROP FOREIGN KEY FK_62615BA4CB7A030');
        $this->addSql('ALTER TABLE matches DROP FOREIGN KEY FK_62615BAEF1DD85D');
        $this->addSql('DROP INDEX IDX_62615BA4CB7A030 ON matches');
        $this->addSql('DROP INDEX IDX_62615BAEF1DD85D ON matches');
        $this->addSql('ALTER TABLE matches ADD equipe_locale_id INT DEFAULT NULL, ADD equipe_adverse_id INT DEFAULT NULL, DROP equipe_locale, DROP equipe_adverse');
        $this->addSql('ALTER TABLE matches ADD CONSTRAINT FK_62615BA28C25BF2 FOREIGN KEY (equipe_adverse_id) REFERENCES equipes (id)');
        $this->addSql('ALTER TABLE matches ADD CONSTRAINT FK_62615BAB928BA2A FOREIGN KEY (equipe_locale_id) REFERENCES equipes (id)');
        $this->addSql('CREATE INDEX IDX_62615BA28C25BF2 ON matches (equipe_adverse_id)');
        $this->addSql('CREATE INDEX IDX_62615BAB928BA2A ON matches (equipe_locale_id)');
    }
}
