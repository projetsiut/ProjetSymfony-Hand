<?php

namespace App\Form;

use App\Entity\Matches;
use App\Entity\Equipes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('equipe_locale', EntityType::class, [
              'class' => Equipes::class,
              'choice_label' => 'libelle',
              'placeholder' =>  'Choisir une equipe'
            ])
            ->add('domicile_exterieur')
            ->add('equipe_adverse')
            ->add('date_heure', DateType::class, [
              'widget' => 'single_text'
            ])
            ->add('num_semaine')
            ->add('num_journee')
            ->add('gymnase')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Matches::class,
        ]);
    }
}
