<?php

namespace App\DataFixtures;

use App\Entity\Equipes;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TeamFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
//        $team01 = new Equipes();
//        $team01->setLibelle('Mini-Hand')
//                ->setEntraineur('Laurent Ponsard / Marine Barnier')
//                ->setCreneaux('Mercredi 16h30-18h (nouveau)')
//                ->setUrlPhoto('MiniHand.jpg')
//                ->setUrlResultCalendrier('')
//                ->setCommentaire('');
//
//        $team02 = new Equipes();
//        $team02->setLibelle('Loisirs')
//            ->setEntraineur('Audrey Dumas')
//            ->setCreneaux('Mercredi 20h-22h (ancien)')
//            ->setUrlPhoto('Loisirs.jpg')
//            ->setUrlResultCalendrier('')
//            ->setCommentaire("Les matches ont lieu en semaine et sont suivis d'une troisième mi - temps conviviale .");
//
//        $team03 = new Equipes();
//        $team03->setLibelle('Moins de 13 A')
//            ->setEntraineur('Laurent Ponsard / Geoffrey Geray')
//            ->setCreneaux('Lundi 17h-18h30 (nouveau), mercredi 16h30-18h (nouveau)')
//            ->setUrlPhoto('Moins13.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/' .
//                'championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_' .
//                'pi1%5Bcompetition_id%5D=10822&tx_obladygesthand_pi1%5Bphase_id%5D=33668&tx_obladygesthand_pi1%5Bgroupe_' .
//                'id%5D=58148&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=2a02538cae028d39fa9ac0a8cef3e8db')
//            ->setCommentaire('Championnat 1ère division AURA, 2ème phase');
//
//        $team04 = new Equipes();
//        $team04->setLibelle('Moins de 15 A')
//            ->setEntraineur('Quentin Tabari / Gabin Bosc / Robert Simon / Geoffrey Geray')
//            ->setCreneaux('Mercredi 18h30-20h (ancien), vendredi 17h-18h15 (alternance)')
//            ->setUrlPhoto('Moins15.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/championnats-' .
//                'regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_' .
//                'id%5D=10821&tx_obladygesthand_pi1%5Bphase_id%5D=33670&tx_obladygesthand_pi1%5Bgroupe_id%5D=58138&tx_' .
//                'obladygesthand_pi1%5Bmode%5D=single_phase&cHash=282c11960d73c66c1b3d3f324c4afabf')
//            ->setCommentaire('Championnat 1ère division AURA, 2ème phase');
//
//        $team05 = new Equipes();
//        $team05->setLibelle('Moins de 15 B')
//            ->setEntraineur('Quentin Tabari / Gabin Bosc / Robert Simon / Geoffrey Geray')
//            ->setCreneaux('Mercredi 18h30-20h (ancien), vendredi 17h-18h15 (alternance)')
//            ->setUrlPhoto('Moins15.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/championnats-regionaux/' .
//                'alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=' .
//                '10821&tx_obladygesthand_pi1%5Bphase_id%5D=33669&tx_obladygesthand_pi1%5Bgroupe_id%5D=58105&tx_' .
//                'obladygesthand_pi1%5Bmode%5D=single_phase&cHash=6a6538eadefdf3fe163d65ab1b2a5550')
//            ->setCommentaire('');
//
//        $team06 = new Equipes();
//        $team06->setLibelle('Moins de 18 A')
//            ->setEntraineur('Quentin Tabari / Geoffrey Geray')
//            ->setCreneaux('Mercredi 18h-20h00 (nouveau), Vendredi 18h15-19h30 (alternance)')
//            ->setUrlPhoto('Moins18.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/championnats-' .
//                'regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_' .
//                'id%5D=10820&tx_obladygesthand_pi1%5Bphase_id%5D=33487&tx_obladygesthand_pi1%5Bgroupe_id' .
//                '%5D=57550&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=b8abdb075bbf8b0ddff9fc4d52c03350')
//            ->setCommentaire('Championnat 1ère division AURA, 2ème phase');
//
//        $team07 = new Equipes();
//        $team07->setLibelle('Seniors prérégionales (SF2)')
//            ->setEntraineur('Christophe Léger')
//            ->setCreneaux('Mercredi 20h-22h (nouveau), vendredi 19h30-21h00 (alternance)')
//            ->setUrlPhoto('Seniors2.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org//competitions/championnats-' .
//                'regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_' .
//                'id%5D=10767&tx_obladygesthand_pi1%5Bphase_id%5D=27392&tx_obladygesthand_pi1%5Bgroupe_id%5D=' .
//                '49971&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=7993b9eeec6ad9bc3145ea6252c7a459');
//
//        $team08 = new Equipes();
//        $team08->setLibelle('Seniors prérégionales (SF1)')
//            ->setEntraineur('Sébastien Chosson / Geoffrey Geray')
//            ->setCreneaux('Mercredi 20h-22h (nouveau), Vendredi 20h30-22h00 (alternance)')
//            ->setUrlPhoto('Seniors1.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org//competitions/championnats' .
//                '-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition' .
//                '_id%5D=10492&tx_obladygesthand_pi1%5Bphase_id%5D=27283&tx_obladygesthand_pi1%5Bgroupe_id%5D=47609&tx' .
//                '_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=6afc829d898e6856985c802f739026d2');
//
//        $team09 = new Equipes();
//        $team09->setLibelle('Moins de 18 B')
//            ->setEntraineur('Quentin Tabari / Geoffrey Geray')
//            ->setCreneaux('Mercredi 18h-20h00 (nouveau), Vendredi 18h15-19h30 (alternance)')
//            ->setUrlPhoto('Moins18.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/championnats-' .
//                'regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition' .
//                '_id%5D=10820&tx_obladygesthand_pi1%5Bphase_id%5D=33492&tx_obladygesthand_pi1%5Bgroupe_id%5D=' .
//                '57572&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=ad0607a15385b6635f5d999864f7d279');
//
//        $team10 = new Equipes();
//        $team10->setLibelle('Moins de 11')
//            ->setEntraineur('Bruno Follonier  / Laurent Thomas')
//            ->setCreneaux('Mardi 17h-18h30 (ancien)')
//            ->setUrlPhoto('Moins11.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/championnats-' .
//                'departementaux/26-comite-de-la-drome.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_' .
//                'pi1%5Bcompetition_id%5D=11517&tx_obladygesthand_pi1%5Bphase_id%5D=32413&tx_obladygesthand_pi1%5' .
//                'Bgroupe_id%5D=57018&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=ae90f305c5d346425065a3fb0e8077a7');
//
//        $team11 = new Equipes();
//        $team11->setLibelle('Moins de 13 B')
//            ->setEntraineur('Laurent Ponsard / Geoffrey Geray')
//            ->setCreneaux('Lundi 17h-18h30 (nouveau), mercredi 16h30-18h (nouveau)')
//            ->setUrlPhoto('Moins13.jpg')
//            ->setUrlResultCalendrier('http://www.ff-handball.org/competitions/championnats-' .
//                'regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition' .
//                '_id%5D=10822&tx_obladygesthand_pi1%5Bphase_id%5D=27422&tx_obladygesthand_pi1%5Bgroupe_id%5D=51174&tx' .
//                '_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=c2d0da2d5c3a28f0defca488b7d31a3c');
//
//
//        $manager->persist($team01);
//        $manager->persist($team02);
//        $manager->persist($team03);
//        $manager->persist($team04);
//        $manager->persist($team05);
//        $manager->persist($team06);
//        $manager->persist($team07);
//        $manager->persist($team08);
//        $manager->persist($team09);
//        $manager->persist($team10);
//        $manager->persist($team11);
//        $manager->flush();
    }
}
