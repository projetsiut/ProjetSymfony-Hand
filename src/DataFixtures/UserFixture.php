<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();

        $marmog = new User();
        $marmog->setUsername('marmorag')
            ->setPassword($this->encoder->encodePassword($marmog,'admin'))
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);

        $grimwalt = new User();
        $grimwalt->setUsername('grimaudc')
            ->setPassword($this->encoder->encodePassword($marmog,'admin'))
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);

        $virgule = new User();
        $virgule->setUsername('lacondev')
            ->setPassword($this->encoder->encodePassword($marmog,'admin'))
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);

        $challute = new User();
        $challute->setUsername('challutc')
            ->setPassword($this->encoder->encodePassword($marmog,'admin'))
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);

        $admin = new User();
        $admin->setUsername('admin')
            ->setPassword($this->encoder->encodePassword($admin,'admin'))
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN']);

        $manager->persist($marmog);
        $manager->persist($challute);
        $manager->persist($grimwalt);
        $manager->persist($virgule);
        $manager->persist($admin);

        $manager->flush();
    }
}
