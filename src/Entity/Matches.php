<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatchesRepository")
 */
class Matches
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Equipes", inversedBy="matches")
     * @JoinColumn(name="equipe_locale", referencedColumnName="id")
     */
    private $equipe_locale;

    /**
     * @ORM\Column(type="smallint")
     */
    private $domicile_exterieur;

    /**
     * @ORM\Column(type="string")
     */
    private $equipe_adverse;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_heure;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $num_semaine;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $num_journee;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $gymnase;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEquipeLocale(): ?Equipes
    {
        return $this->equipe_locale;
    }

    public function setEquipeLocale(?Equipes $equipe_locale): self
    {
        $this->equipe_locale = $equipe_locale;

        return $this;
    }

    public function getDomicileExterieur(): ?int
    {
        return $this->domicile_exterieur;
    }

    public function setDomicileExterieur(int $domicile_exterieur): self
    {
        $this->domicile_exterieur = $domicile_exterieur;

        return $this;
    }

    public function getEquipeAdverse(): ?String
    {
        return $this->equipe_adverse;
    }

    public function setEquipeAdverse(String $equipe_adverse): self
    {
        $this->equipe_adverse = $equipe_adverse;

        return $this;
    }

    public function getDateHeure(): ?\DateTimeInterface
    {
        return $this->date_heure;
    }

    public function setDateHeure(?\DateTimeInterface $date_heure): self
    {
        $this->date_heure = $date_heure;

        return $this;
    }

    public function getNumSemaine(): ?int
    {
        return $this->num_semaine;
    }

    public function setNumSemaine(?int $num_semaine): self
    {
        $this->num_semaine = $num_semaine;

        return $this;
    }

    public function getNumJournee(): ?int
    {
        return $this->num_journee;
    }

    public function setNumJournee(?int $num_journee): self
    {
        $this->num_journee = $num_journee;

        return $this;
    }

    public function getGymnase(): ?string
    {
        return $this->gymnase;
    }

    public function setGymnase(?string $gymnase): self
    {
        $this->gymnase = $gymnase;

        return $this;
    }
}
