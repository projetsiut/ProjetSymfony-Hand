<?php

namespace App\Controller;

use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\MatchesRepository;

class AgendaController extends AbstractController
{
    /**
     * @Route("/agenda", name="agenda", methods={"GET"})
     */
    public function index(MatchesRepository $matchesRepository): \Symfony\Component\HttpFoundation\Response
    {
        return $this->render('agenda/index.html.twig', [
            'matches' => $matchesRepository->findByNumSemaine('42'),
        ]);
    }
}
