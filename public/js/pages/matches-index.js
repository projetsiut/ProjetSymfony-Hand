$(document).ready(function() {
    $('#matches-table').DataTable({
            language: {
                url: '/lang/datatable.french.json'
            },
        buttons: [
            {
                html: "<i class='fas fa-plus-square'></i>",
                action: function (e, dt, node, config)
                {
                    //This will send the page to the location specified
                    window.location.href = '/new/';
                }
            }
        ]
        });
} );
