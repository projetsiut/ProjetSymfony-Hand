-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  mar. 09 avr. 2019 à 16:28
-- Version du serveur :  10.1.26-MariaDB-0+deb9u1
-- Version de PHP :  7.2.11-2+0~20181015120801.9+stretch~1.gbp8105e0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `marmorag`
--

-- --------------------------------------------------------

--
-- Structure de la table `equipes`
--

CREATE TABLE `equipes` (
  `id` int(11) NOT NULL,
  `libelle` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entraineur` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creneaux` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_result_calendrier` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `equipes`
--

INSERT INTO `equipes` (`id`, `libelle`, `entraineur`, `creneaux`, `url_photo`, `url_result_calendrier`, `commentaire`) VALUES
(1, 'Mini-Hand', 'Laurent Ponsard / Marine Barnier', 'Mercredi 16h30-18h (nouveau)', 'MiniHand.jpg', '', ''),
(2, 'Loisirs', 'Audrey Dumas', 'Mercredi 20h-22h (ancien)', 'Loisirs.jpg', '', 'Les matches ont lieu en semaine et sont suivis d\'une troisième mi-temps conviviale.'),
(3, 'Moins de 13 A', 'Laurent Ponsard / Geoffrey Geray', 'Lundi 17h-18h30 (nouveau), mercredi 16h30-18h (nouveau)', 'Moins13.jpg', 'http://www.ff-handball.org/competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10822&tx_obladygesthand_pi1%5Bphase_id%5D=33668&tx_obladygesthand_pi1%5Bgroupe_id%5D=58148&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=2a02538cae028d39fa9ac0a8cef3e8db', 'Championnat 1ère division AURA, 2ème phase'),
(4, 'Moins de 15 A', 'Quentin Tabari / Gabin Bosc / Robert Simon / Geoffrey Geray', 'Mercredi 18h30-20h (ancien), vendredi 17h-18h15 (alternance)', 'Moins15.jpg', 'http://www.ff-handball.org/competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10821&tx_obladygesthand_pi1%5Bphase_id%5D=33670&tx_obladygesthand_pi1%5Bgroupe_id%5D=58138&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=282c11960d73c66c1b3d3f324c4afabf', 'Championnat 1ère division AURA, 2ème phase'),
(5, 'Moins de 15 B', 'Quentin Tabari / Gabin Bosc / Robert Simon / Geoffrey Geray', 'Mercredi 18h30-20h (ancien), vendredi 17h-18h15 (alternance)', 'Moins15.jpg', 'http://www.ff-handball.org/competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10821&tx_obladygesthand_pi1%5Bphase_id%5D=33669&tx_obladygesthand_pi1%5Bgroupe_id%5D=58105&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=6a6538eadefdf3fe163d65ab1b2a5550', 'Championnat 1ère division AURA, 2ème phase'),
(6, 'Moins de 18 A', 'Quentin Tabari / Geoffrey Geray', 'Mercredi 18h-20h00 (nouveau), Vendredi 18h15-19h30 (alternance)', 'Moins18.jpg', 'http://www.ff-handball.org/competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10820&tx_obladygesthand_pi1%5Bphase_id%5D=33487&tx_obladygesthand_pi1%5Bgroupe_id%5D=57550&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=b8abdb075bbf8b0ddff9fc4d52c03350', 'Championnat 1ère division AURA, 2ème phase'),
(7, 'Seniors prérégionales (SF2)', 'Christophe Léger', 'Mercredi 20h-22h (nouveau), vendredi 19h30-21h00 (alternance)', 'Seniors2.jpg', 'http://www.ff-handball.org//competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10767&tx_obladygesthand_pi1%5Bphase_id%5D=27392&tx_obladygesthand_pi1%5Bgroupe_id%5D=49971&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=7993b9eeec6ad9bc3145ea6252c7a459', '1ère division AURA en plus de 16 ans'),
(8, 'Seniors prénationales (SF1)', 'Sébastien Chosson / Geoffrey Geray', 'Mercredi 20h-22h (nouveau), Vendredi 20h30-22h00 (alternance)', 'Seniors1.jpg', 'http://www.ff-handball.org//competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10492&tx_obladygesthand_pi1%5Bphase_id%5D=27283&tx_obladygesthand_pi1%5Bgroupe_id%5D=47609&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=6afc829d898e6856985c802f739026d2', 'Équipe fanion, joue en prénational, poule 4'),
(9, 'Moins de 18 B', 'Quentin Tabari / Geoffrey Geray', 'Mercredi 18h-20h00 (nouveau), Vendredi 18h15-19h30 (alternance)', 'Moins18.jpg', 'http://www.ff-handball.org/competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10820&tx_obladygesthand_pi1%5Bphase_id%5D=33492&tx_obladygesthand_pi1%5Bgroupe_id%5D=57572&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=ad0607a15385b6635f5d999864f7d279', 'Championnat 1ère division AURA, 2ème phase'),
(10, 'Moins de 11', 'Bruno Follonier  / Laurent Thomas', 'Mardi 17h-18h30 (ancien)', 'Moins11.jpg', 'http://www.ff-handball.org/competitions/championnats-departementaux/26-comite-de-la-drome.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=11517&tx_obladygesthand_pi1%5Bphase_id%5D=32413&tx_obladygesthand_pi1%5Bgroupe_id%5D=57018&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=ae90f305c5d346425065a3fb0e8077a7', '2ème phase, découverte'),
(11, 'Moins de 13 B', 'Laurent Ponsard / Geoffrey Geray', 'Lundi 17h-18h30 (nouveau), mercredi 16h30-18h (nouveau)', 'Moins13.jpg', 'http://www.ff-handball.org/competitions/championnats-regionaux/alsace.html?tx_obladygesthand_pi1%5Bsaison_id%5D=14&tx_obladygesthand_pi1%5Bcompetition_id%5D=10822&tx_obladygesthand_pi1%5Bphase_id%5D=27422&tx_obladygesthand_pi1%5Bgroupe_id%5D=51174&tx_obladygesthand_pi1%5Bmode%5D=single_phase&cHash=c2d0da2d5c3a28f0defca488b7d31a3c', 'Phase de brassage, première division AURA');

-- --------------------------------------------------------

--
-- Structure de la table `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `domicile_exterieur` smallint(6) NOT NULL,
  `date_heure` datetime DEFAULT NULL,
  `num_semaine` int(11) DEFAULT NULL,
  `num_journee` int(11) DEFAULT NULL,
  `gymnase` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipe_locale` int(11) DEFAULT NULL,
  `equipe_adverse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `matches`
--

INSERT INTO `matches` (`id`, `domicile_exterieur`, `date_heure`, `num_semaine`, `num_journee`, `gymnase`, `equipe_locale`, `equipe_adverse`) VALUES
(1, 1, '2018-09-26 20:00:00', 39, 0, NULL, 2, 'Vernoux'),
(2, 0, '2018-09-16 16:00:00', 37, 1, 'GYMNASE DES Ã‰COLES, CHABEUIL', 8, 'HBC CHABEUIL'),
(3, 0, '2018-10-06 19:00:00', 40, 3, 'GYMNASE MUNICIPAL, ST DONAT SUR L HERBASSE', 8, 'ST DONAT SF1'),
(4, 1, '2018-10-13 20:00:00', 41, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'Ardèche Le Pouzin HB07  3'),
(5, 0, '2018-11-11 11:00:00', 45, 6, 'Complexe omnisports de Choumouroux, YSSINGEAUX', 8, 'Handball Sucs Beaux Yssingeaux'),
(6, 1, '2018-11-18 16:00:00', 46, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'AL Ricamarie'),
(7, 0, '2018-11-25 16:00:00', 47, 8, 'G3 Les Bruneaux, FIRMINY', 8, 'FIRMINY VALLEE DE L\'ONDAINE HANDBALL'),
(8, 1, '2019-01-12 21:00:00', 2, 9, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'PORTE DE L\'ISERE (Villefontaine) 2'),
(9, 0, '2019-01-20 16:00:00', 3, 10, 'gymnase du MAZEL, MONISTROL SUR LOIRE', 8, 'ENTENTE ST ETIENNE METROPOLE / MONTEIL'),
(10, 0, '2019-01-26 20:30:00', 4, 11, 'MICHEL BETTON, ST VALLIER', 8, 'Athlétic Handball St Vallier'),
(11, 1, '2019-02-02 20:30:00', 5, 12, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'HBC CHABEUIL'),
(12, 1, '2019-03-10 16:00:00', 10, 14, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'ST DONAT SF1'),
(13, 0, '2019-03-16 18:15:00', 11, 15, 'JACKSON RICHARDSON, LE POUZIN', 8, 'Ardèche Le Pouzin HB07  3'),
(14, 1, '2019-03-30 21:00:00', 13, 17, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'Handball Sucs Beaux Yssingeaux'),
(15, 0, '2019-04-06 20:00:00', 14, 18, 'JULES VALLES, LA RICAMARIE', 8, 'AL Ricamarie'),
(16, 1, '2019-04-14 14:00:00', 15, 19, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'FIRMINY VALLEE DE L\'ONDAINE HANDBALL'),
(17, 0, '0000-00-00 00:00:00', 18, 20, ', ', 8, 'PORTE DE L\'ISERE (Villefontaine) 2'),
(18, 1, '2019-05-12 11:00:00', 19, 21, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'ENTENTE ST ETIENNE METROPOLE / MONTEIL'),
(19, 1, '2019-05-18 19:00:00', 20, 22, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 8, 'Athlétic Handball St Vallier'),
(20, 0, '2018-09-15 14:00:00', 37, 1, 'GYMNASE PIERRE DE COUBERTIN (LA VIOLETTE), LE TEIL', 6, 'HB 07 LE TEIL 1'),
(21, 1, '2018-09-22 17:00:00', 38, 2, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'HB ETOILE/BEAUVALLON'),
(22, 0, '2018-09-29 17:00:00', 39, 3, 'GYMNASE ROQUA, AUBENAS', 6, 'ESE AUBENAS'),
(23, 0, '2019-03-10 14:00:00', 10, 5, 'GYMNASE CLAUDE BON, LIVRON SUR DROME', 6, 'LIVRON HANDBALL'),
(24, 1, '2018-11-11 11:00:00', 45, 6, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'HB 07 LE TEIL 1'),
(25, 0, '2018-11-17 16:00:00', 46, 7, 'GYMNASE MUNICIPAL, ETOILE SUR RHONE', 6, 'HB ETOILE/BEAUVALLON'),
(26, 1, '2018-11-24 16:00:00', 47, 8, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'ESE AUBENAS'),
(27, 1, '2019-05-19 11:00:00', 20, 10, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'LIVRON HANDBALL'),
(28, 1, '2018-09-15 16:00:00', 37, 1, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'HB 07 LE TEIL 1'),
(29, 0, '2018-09-22 16:00:00', 38, 2, 'GYMNASE DU ZODIAQUE, ANNONAY', 4, 'HBC ANNONEEN'),
(30, 0, '2018-10-06 17:00:00', 40, 4, 'GYMNASE CLAUDE BON, LIVRON SUR DROME', 4, 'LIVRON HANDBALL'),
(31, 1, '2018-10-13 17:30:00', 41, 5, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'ARDECHE LE POUZIN HB07 Handball Féminin'),
(32, 0, '2018-10-20 18:00:00', 42, 6, 'GYMNASE PIERRE DE COUBERTIN (LA VIOLETTE), LE TEIL', 4, 'HB 07 LE TEIL 1'),
(33, 1, '2018-11-10 14:30:00', 45, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'HBC ANNONEEN'),
(34, 1, '2018-12-01 16:45:00', 48, 9, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'LIVRON HANDBALL'),
(35, 0, '2018-12-08 16:00:00', 49, 10, 'JACKSON RICHARDSON, LE POUZIN', 4, 'ARDECHE LE POUZIN HB07 Handball Féminin'),
(36, 1, '2018-09-15 14:00:00', 37, 1, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'Bourg de Péage Drôme Handball'),
(37, 0, '2018-09-22 13:00:00', 38, 2, 'GYMNASE DU ZODIAQUE, ANNONAY', 3, 'HBCA -13 F EXC AURA'),
(38, 1, '2018-09-29 14:00:00', 39, 3, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'pays voironnais handball'),
(39, 0, '2018-10-06 15:30:00', 40, 4, 'GYMNASE CLAUDE BON, LIVRON SUR DROME', 3, 'LIVRON HANDBALL'),
(40, 1, '2018-10-20 14:30:00', 42, 5, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'Ardèche Le Pouzin HB07 Handball Féminin'),
(41, 0, '2018-11-10 16:00:00', 45, 6, 'COMPLEXE SPORTIF VERCORS / SALLE COMPETITION, BOURG DE PEAGE', 3, 'Bourg de Péage Drôme Handball'),
(42, 1, '2018-11-18 11:00:00', 46, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'HBCA -13 F EXC AURA'),
(43, 0, '2018-11-25 11:00:00', 47, 8, 'Gymnase Le vergeron, MOIRANS', 3, 'pays voironnais handball'),
(44, 1, '2018-12-01 13:00:00', 48, 9, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'LIVRON HANDBALL'),
(45, 0, '2018-12-08 14:00:00', 49, 10, 'JACKSON RICHARDSON, LE POUZIN', 3, 'Ardèche Le Pouzin HB07 Handball Féminin'),
(46, 0, '2018-09-22 20:00:00', 38, 1, 'MICHEL BETTON, ST VALLIER', 7, 'AHB ST VALLIER 2'),
(47, 1, '2018-10-07 11:00:00', 40, 2, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'DROME HANDBALL BOURG DE PEAGE 3'),
(48, 0, '2018-10-13 20:30:00', 41, 3, 'GYMNASE MUNICIPAL, ST DONAT SUR L HERBASSE', 7, 'ST DONAT 2'),
(49, 1, '2018-10-21 11:00:00', 42, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'HB TAIN VION TOURNON'),
(50, 0, '2018-11-10 13:45:00', 45, 5, 'SALLE OMNISPORTS, ST MAURICE L EXIL', 7, 'ST MAURICE L\'EXIL '),
(51, 1, '2018-11-18 14:00:00', 46, 6, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'HBC CHABEUIL 2'),
(52, 0, '2018-11-25 14:00:00', 47, 7, 'GYMNASE JOSEPH PLAT, SALAISE SUR SANNE', 7, 'RHODIA'),
(53, 1, '2018-12-08 21:00:00', 49, 8, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'HANDBALL CLUB LAMASTROIS'),
(54, 0, '2019-01-12 18:30:00', 2, 9, 'GYMNASE DU ZODIAQUE, ANNONAY', 7, 'HBC ANNONEEN '),
(55, 1, '2019-02-23 20:00:00', 8, 10, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'AHB ST VALLIER 2'),
(56, 0, '2019-01-27 14:30:00', 4, 11, 'COMPLEXE SPORTIF VERCORS / SALLE COMPETITION, BOURG DE PEAGE', 7, 'DROME HANDBALL BOURG DE PEAGE 3'),
(57, 1, '2019-02-09 21:00:00', 6, 12, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'ST DONAT 2'),
(58, 0, '2019-03-09 20:00:00', 10, 13, 'HALLE DES SPORTS L. SAUSSET, TOURNON SUR RHONE', 7, 'HB TAIN VION TOURNON'),
(59, 1, '2019-03-17 16:00:00', 11, 14, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'ST MAURICE L\'EXIL '),
(60, 0, '2019-03-23 17:00:00', 12, 15, 'GYMNASE DES Ã‰COLES, CHABEUIL', 7, 'HBC CHABEUIL 2'),
(61, 1, '2019-03-30 19:00:00', 13, 16, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'RHODIA'),
(62, 0, '2019-04-06 20:00:00', 14, 17, 'GYMNASE INTERCOMMUNAL, LAMASTRE', 7, 'HANDBALL CLUB LAMASTROIS'),
(63, 1, '2019-05-05 14:00:00', 18, 18, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 7, 'HBC ANNONEEN '),
(64, 1, '2018-09-29 16:00:00', 39, 1, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 11, 'HBC CHABEUIL'),
(66, 1, '2018-10-27 14:30:00', 43, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 11, 'BOURG DE PEAGE DROME HANDBALL 2'),
(68, 0, '2018-11-17 14:00:00', 46, 6, 'GYMNASE DES Ã‰COLES, CHABEUIL', 11, 'HBC CHABEUIL'),
(70, 0, '2018-12-08 15:00:00', 49, 9, 'COMPLEXE SPORTIF VERCORS / SALLE COMPETITION, BOURG DE PEAGE', 11, 'BOURG DE PEAGE DROME HANDBALL 2'),
(73, 1, '2018-10-06 15:00:00', 40, 2, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'St Donat -15 F'),
(75, 0, '2018-10-20 15:15:00', 42, 4, 'SALLE OMNISPORTS, ST RAMBERT D ALBON', 5, 'RHODIA (Isardrome)'),
(77, 0, '2018-11-17 17:15:00', 46, 7, 'GYMNASE MUNICIPAL, ST DONAT SUR L HERBASSE', 5, 'St Donat -15 F'),
(79, 1, '2018-12-08 18:30:00', 49, 9, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'RHODIA (Isardrome)'),
(81, 0, '2018-10-20 18:00:00', 42, 2, 'Gymnase municipal, ETOILE SUR RHONE', 9, 'HB ETOILE/BEAUVALLON'),
(84, 1, '2018-12-01 18:45:00', 48, 5, 'Centre Omnisports, GUILHERAND GRANGES', 9, 'HB ETOILE/BEAUVALLON'),
(86, 1, '2018-10-20 13:00:00', 42, 3, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 11, 'HB PAYS DE ST MARCELLIN'),
(88, 0, '2018-12-01 15:00:00', 48, 8, 'Gymnase Carrier, ST MARCELLIN', 11, 'HB PAYS DE ST MARCELLIN'),
(90, 0, '2018-10-13 13:30:00', 41, 3, 'Halle des Sports, ST MARCEL LES VALENCE', 5, 'ENT. SAINT MARCELLOIS-BOURG DE PEAGE'),
(92, 1, '2018-10-06 17:00:00', 40, 1, 'Centre Omnisports, GUILHERAND GRANGES', 9, 'ENT.  BOURG LES VALENCE-ST MARCELLOIS'),
(93, 0, '2018-11-17 17:00:00', 46, 4, 'Halle du Valentin, BOURG LES VALENCE', 9, 'ENT.  BOURG LES VALENCE-ST MARCELLOIS'),
(94, 1, '2018-10-20 11:00:00', 42, 1, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 10, 'Handball Club St Marcellois'),
(95, 0, '2018-11-17 14:30:00', 46, 2, 'GYMNASE MUNICIPAL, ETOILE SUR RHONE', 10, 'HB ETOILE/BEAUVALLON'),
(96, 0, '2018-12-01 13:45:00', 48, 3, 'GYMNASE CLAUDE BON, LIVRON SUR DROME', 10, 'LIVRON HANDBALL'),
(97, 0, '2018-10-09 20:00:00', 41, 0, NULL, 2, 'Les Ollières'),
(98, 0, '2018-10-11 20:00:00', 41, 0, NULL, 2, 'Étoile'),
(99, 1, '2019-04-10 20:30:00', 15, 0, NULL, 2, 'Privas'),
(100, 0, '2018-12-06 20:00:00', 49, 0, NULL, 2, 'Bourg-de-Péage'),
(101, 1, '2018-10-06 17:00:00', 40, 1, 'Centre Omnisports, GUILHERAND GRANGES', 9, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(102, 0, '2018-11-17 17:00:00', 46, 4, 'Halle du Valentin, BOURG LES VALENCE', 9, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(103, 1, '2018-11-01 08:00:00', 44, 0, NULL, 2, 'Tournoi Halloween'),
(104, 0, '2018-11-10 14:00:00', 45, 5, 'Halle des Sports, ST MARCEL LES VALENCE', 11, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(105, 1, '2018-12-15 17:00:00', 50, 10, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 11, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(106, 0, '2018-11-04 14:00:00', 44, 0, NULL, 8, 'Livron'),
(107, 1, '2018-11-14 20:00:00', 46, 0, NULL, 2, 'Livron'),
(108, 0, '2018-11-15 20:15:00', 46, 0, NULL, 2, 'Loriol'),
(109, 1, '2019-02-02 18:00:00', 5, 3, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(110, 0, '2019-04-06 14:00:00', 14, 8, 'Halle des Sports, ST MARCEL LES VALENCE', 5, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(111, 1, '2019-03-20 20:30:00', 12, 0, NULL, 2, 'Charmes'),
(112, 0, '2018-11-27 20:00:00', 48, 0, NULL, 2, 'Vernoux'),
(113, 1, '2019-02-06 20:00:00', 6, 0, NULL, 2, 'Valence mairie'),
(114, 1, '2018-12-12 20:00:00', 50, 0, 'Ancien', 2, 'HBGG'),
(115, 0, '2019-02-07 20:30:00', 6, 0, NULL, 2, 'Romans'),
(116, 0, '2019-01-07 20:00:00', 2, 0, NULL, 2, 'Chomérac'),
(117, 1, '2019-01-09 20:00:00', 2, 0, NULL, 2, 'Die'),
(118, 1, '2019-01-23 20:00:00', 4, 0, 'Ancien', 2, 'Étoile'),
(119, 0, '2019-01-24 20:30:00', 4, 0, NULL, 2, 'Montélimar'),
(120, 0, '2019-01-19 14:00:00', 3, 1, 'COMPLEXE SPORTIF VERCORS / SALLE COMPETITION, BOURG DE PEAGE', 10, 'BOURG DE PEAGE'),
(121, 0, '2019-02-02 14:45:00', 5, 2, 'MICHEL BETTON, ST VALLIER', 10, 'ST VALLIER'),
(122, 1, '2019-03-10 11:00:00', 10, 3, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 10, 'ST RAMBERT HB'),
(123, 0, '2019-03-23 15:00:00', 12, 4, 'GYMNASE CLAUDE BON, LIVRON SUR DROME', 10, 'ST VALLIER et LIVRON HANDBALL'),
(124, 1, '2019-04-06 13:30:00', 14, 5, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 10, 'ETOILE/BEAUVALLON 2'),
(125, 0, '0000-00-00 00:00:00', 19, 6, 'SALLE OMNISPORTS, ST RAMBERT D ALBON', 10, 'LIVRON HANDBALL et ST RAMBERT HB'),
(126, 0, '0000-00-00 00:00:00', 21, 7, 'GYMNASE MUNICIPAL, ETOILE SUR RHONE', 10, 'BOURG DE PEAGE et ETOILE/BEAUVALLON 1'),
(127, 1, '2019-01-12 19:00:00', 2, 1, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'HB ETOILE/BEAUVALLON'),
(128, 0, '2019-01-19 13:45:00', 3, 2, 'LA GARENNE, SUZE LA ROUSSE', 6, 'HBC SUZE LA ROUSSE'),
(129, 0, '2019-01-27 11:00:00', 4, 3, 'GYMNASE ROQUA, AUBENAS', 6, 'ENTENTE ST ETIENNE AUBENAS HANDBALL'),
(130, 1, '2019-02-09 19:00:00', 6, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'BOURG DE PEAGE DROME HANDBALL 2'),
(131, 0, '2019-03-16 17:00:00', 11, 6, 'GYMNASE MUNICIPAL, ETOILE SUR RHONE', 6, 'HB ETOILE/BEAUVALLON'),
(132, 1, '2019-03-23 19:00:00', 12, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'HBC SUZE LA ROUSSE'),
(133, 1, '2019-03-30 17:00:00', 13, 8, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'ENTENTE ST ETIENNE AUBENAS HANDBALL'),
(134, 0, '0000-00-00 00:00:00', 18, 9, ', ', 6, 'BOURG DE PEAGE DROME HANDBALL 2'),
(135, 0, '2019-01-12 16:00:00', 2, 1, 'ESPACE EDUCATIF ET SPORTIF, MONTELIMAR', 9, 'MONTELIMAR CLUB HANDBALL'),
(136, 0, '2019-01-20 11:00:00', 3, 2, 'HALLE DES SPORTS, PIERRELATTE', 9, 'ASE TRICASTIN HB'),
(137, 0, '2019-01-26 17:00:00', 4, 3, 'GYMNASE DE L\'EYRIEUX, ST SAUVEUR DE MONTAGUT', 9, 'AS LES OLLIERES HB (ASO HANDBALL)'),
(138, 1, '2019-03-10 14:00:00', 10, 5, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 9, 'AS RUOMS VALLON HANDBALL'),
(139, 1, '2019-03-17 14:00:00', 11, 6, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 9, 'MONTELIMAR CLUB HANDBALL'),
(140, 1, '2019-03-23 17:00:00', 12, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 9, 'ASE TRICASTIN HB'),
(141, 1, '2019-03-30 15:00:00', 13, 8, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 9, 'AS LES OLLIERES HB (ASO HANDBALL)'),
(142, 0, '0000-00-00 00:00:00', 20, 10, ', ', 9, 'AS RUOMS VALLON HANDBALL'),
(143, 1, '2019-01-13 11:00:00', 2, 1, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'HBC LORIOL'),
(144, 0, '2019-01-19 15:15:00', 3, 2, 'Gymnase de Maclas, MACLAS', 4, 'HANDBALL CLUB DU PILAT'),
(145, 0, '2019-01-27 11:00:00', 4, 3, 'COMPLEXE SPORTIF VERCORS / SALLE COMPETITION, BOURG DE PEAGE', 4, 'BOURG DE PEAGE DROME HANDBALL 2'),
(146, 1, '2019-02-10 14:00:00', 6, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'Echalas -15F'),
(147, 0, '2019-03-09 13:30:00', 10, 5, 'GYMNASE MUNICIPAL, ST DONAT SUR L HERBASSE', 4, 'St Donat -15 F'),
(148, 0, '2019-03-16 16:00:00', 11, 6, 'GYMNASE DES ECOLES, LORIOL SUR DROME', 4, 'HBC LORIOL'),
(149, 1, '2019-03-23 13:30:00', 12, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'HANDBALL CLUB DU PILAT'),
(150, 1, '2019-04-07 11:00:00', 14, 8, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'BOURG DE PEAGE DROME HANDBALL 2'),
(151, 0, '0000-00-00 00:00:00', 18, 9, ', ', 4, 'Echalas -15F'),
(152, 1, '2019-05-19 14:00:00', 20, 10, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 4, 'St Donat -15 F'),
(153, 0, '2019-01-12 18:30:00', 2, 1, 'GYMNASE MUNICIPAL, ETOILE SUR RHONE', 5, 'HB ETOILE/BEAUVALLON'),
(154, 0, '2019-01-19 14:30:00', 3, 2, 'ESPACE EDUCATIF ET SPORTIF, MONTELIMAR', 5, 'MONTELIMAR CLUB HANDBALL'),
(155, 0, '2019-02-09 16:30:00', 6, 4, 'GYMNASE DE VALS LES BAINS, VALS LES BAINS', 5, 'Ardeche Méridionale HB'),
(156, 1, '2019-03-10 11:00:00', 10, 5, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'HB 07 LE TEIL'),
(157, 1, '2019-03-17 11:00:00', 11, 6, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'HB ETOILE/BEAUVALLON'),
(158, 1, '2019-03-23 15:15:00', 12, 7, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'MONTELIMAR CLUB HANDBALL'),
(159, 1, '2019-05-05 11:00:00', 18, 9, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 5, 'Ardeche Méridionale HB'),
(160, 0, '2019-05-18 16:00:00', 20, 10, 'GYMNASE PIERRE DE COUBERTIN (LA VIOLETTE), LE TEIL', 5, 'HB 07 LE TEIL'),
(161, 0, '2019-01-13 10:00:00', 2, 1, 'GYMNASE PIERRE DE COUBERTIN (LA VIOLETTE), LE TEIL', 3, 'HB 07 LE TEIL'),
(162, 1, '2019-01-27 14:00:00', 4, 2, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'MONTELIMAR CLUB HANDBALL'),
(163, 0, '2019-02-02 15:30:00', 5, 3, 'Halle des Sports, ST MARCEL LES VALENCE', 3, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(164, 1, '2019-02-09 17:15:00', 6, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'HB Rhône Eyrieux Ardèche'),
(165, 0, '2019-03-09 16:30:00', 10, 5, 'GYMNASE MUNICIPAL, ETOILE SUR RHONE', 3, 'HB ETOILE/BEAUVALLON'),
(166, 1, '2019-03-16 13:15:00', 11, 6, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'HB 07 LE TEIL'),
(167, 0, '2019-03-24 14:00:00', 12, 7, 'ESPACE EDUCATIF ET SPORTIF, MONTELIMAR', 3, 'MONTELIMAR CLUB HANDBALL'),
(168, 1, '2019-04-06 15:30:00', 14, 8, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'ENTENTE ST MARCEL LES VALENCE-BOURG LES VALENCE'),
(169, 0, '0000-00-00 00:00:00', 18, 9, 'GYMNASE DES GONNETTES, LA VOULTE SUR RHONE', 3, 'HB Rhône Eyrieux Ardèche'),
(170, 1, '2019-05-19 11:00:00', 20, 10, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 3, 'HB ETOILE/BEAUVALLON'),
(171, 1, '2019-02-09 19:00:00', 6, 4, 'CENTRE OMNISPORTS, GUILHERAND GRANGES', 6, 'BOURG DE PEAGE DROME HANDBALL 1'),
(172, 0, '0000-00-00 00:00:00', 18, 9, ', ', 6, 'BOURG DE PEAGE DROME HANDBALL 1'),
(173, 0, '2019-03-01 20:30:00', 9, 0, NULL, 2, 'Bourg-les-Valence'),
(174, 0, '2019-02-27 20:30:00', 9, 0, NULL, 2, 'Chabeuil'),
(175, 0, '2019-02-19 00:00:00', 8, 0, '', 2, 'Vernoux (ANNULÉ, pb de gymnase)'),
(176, 0, '2019-02-21 20:30:00', 8, 0, NULL, 2, 'Valence HB'),
(177, 1, '2019-04-03 20:30:00', 14, 0, NULL, 2, 'Chateuneuf/Isère');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `roles`, `password`) VALUES
(1, 'marmorag', '[\"ROLE_USER\",\"ROLE_ADMIN\"]', '$2y$13$YjMvReBEfFK12XEa8Ld.oe3bdQVEdkJE0qzYh.TpLEDFjS13i3rGm'),
(2, 'challutc', '[\"ROLE_USER\",\"ROLE_ADMIN\"]', '$2y$13$Jb8f4qfQYVrTDKgeJh.9Z.KXQjYpvkLr96K97.SPHKXFV9RlhpOcq'),
(3, 'grimaudc', '[\"ROLE_USER\",\"ROLE_ADMIN\"]', '$2y$13$jt1U3BUQFj8K9dw15XOXBeWO3BCy8vgDnh3udgAmn0EeiE97fFW/e'),
(4, 'lacondev', '[\"ROLE_USER\",\"ROLE_ADMIN\"]', '$2y$13$mf4AEKYwzNIeYQ4d.3YfqupLmYKdbbMmLDIJcrRIZnp6v78AI2bSK');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `equipes`
--
ALTER TABLE `equipes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_62615BA4CB7A030` (`equipe_locale`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `equipes`
--
ALTER TABLE `equipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `matches`
--
ALTER TABLE `matches`
  ADD CONSTRAINT `FK_62615BA4CB7A030` FOREIGN KEY (`equipe_locale`) REFERENCES `equipes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
