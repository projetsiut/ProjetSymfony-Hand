# Projet Symfony - Hand #

Groupe : 

- Virgil Lacondemine
- Clément Challut
- Cédric Grimaud
- Guillaume Marmorat

Install : 

Remplir le .env avec :

    DATABASE_URL=mysql://user:password@127.0.0.1:3306/db_name

Executer :

    composer install
    bin/console doctrine:database:create

Pour charger les utilisateurs
 
    bin/console doctrine:fixture:load

Pour charger les données

    mysql < ./data/Donnees.sql

Enfin

    bin/console server:start